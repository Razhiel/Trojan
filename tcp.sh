#!/usr/bin/env bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

#=================================================
#	System Required: CentOS 6/7/8,Debian 8/9/10,ubuntu 16/18/19
#	Description: BBR+BBRplus+Lotserver
#	Version: 1.3.2.20
#	Author: kenzo
#=================================================

sh_ver="1.3.2.20"
github="github.000060000.xyz"

Green_font_prefix="\033[32m" && Red_font_prefix="\033[31m" && Green_background_prefix="\033[42;37m" && Red_background_prefix="\033[41;37m" && Font_color_suffix="\033[0m"
Info="${Green_font_prefix}[información]${Font_color_suffix}"
Error="${Red_font_prefix}[error]${Font_color_suffix}"
Tip="${Green_font_prefix}[Nota]${Font_color_suffix}"

# Installation BBR kernel
installbbr(){
	kernel_version="4.11.8"
	bit=`uname -m`
	rm -rf bbr
	mkdir bbr && cd bbr
	
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "6" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O kernel-headers-c6.rpm https://github.com/ylx2016/kernel/releases/download/5.5.5/kernel-headers-5.5.5-1-c6.x86_64.rpm
				wget -N -O kernel-c6.rpm https://github.com/ylx2016/kernel/releases/download/5.5.5/kernel-5.5.5-1-c6.x86_64.rpm
			
				yum install -y kernel-c6.rpm
				yum install -y kernel-headers-c6.rpm
			
				kernel_version="5.5.5"
			else
				echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
			fi
		
		elif [[ ${version} = "7" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O kernel-headers-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/ESP6BPY8mhhOn6S3TJQEUdQBOVRbrQ_Sy5T681epEc6c7g?download=1
				wget -N -O kernel-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/Ed_0whzp_VRCkw2oJbXR0W0BFGep10U3-Lridbqwe9Lv5g?download=1

				yum install -y kernel-c7.rpm
				yum install -y kernel-headers-c7.rpm
			
				kernel_version="5.5.10"
			else
				echo -e "${Error} is still using a 32-bit kernel, don't see you again! " && exit 1
			fi	
			
		elif [[ ${version} = "8" ]]; then
			wget -N -O kernel-c8.rpm https://github.com/ylx2016/kernel/releases/download/5.5.5/kernel-5.5.5-1-c8.x86_64.rpm
			wget -N -O kernel-headers-c8.rpm https://github.com/ylx2016/kernel/releases/download/5.5.5/kernel-headers-5.5.5-1-c8.x86_64.rpm

			yum install -y kernel-c8.rpm
			yum install -y kernel-headers-c8.rpm
			
			kernel_version="5.5.5"
		fi
	
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		if [[ "${release}" == "debian" ]]; then
			if [[ ${version} = "8" ]]; then
				if [[ ${bit} = "x86_64" ]]; then
					wget -N -O linux-image-d8.deb https://github.com/ylx2016/kernel/releases/download/5.5.5/linux-image-5.5.5_5.5.5-1-d8_amd64.deb
					wget -N -O linux-headers-d8.deb https://github.com/ylx2016/kernel/releases/download/5.5.5/linux-headers-5.5.5_5.5.5-1-d8_amd64.deb
				
					dpkg -i linux-image-d8.deb
					dpkg -i linux-headers-d8.deb
				
					kernel_version="5.5.5"
				else
					echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
				fi
		
			elif [[ ${version} = "9" ]]; then
				if [[ ${bit} = "x86_64" ]]; then
					wget -N -O linux-image-d9.deb https://github.com/ylx2016/kernel/releases/download/5.5.5/linux-image-5.5.5_5.5.5-1-d9_amd64.deb
					wget -N -O linux-headers-d9.deb https://github.com/ylx2016/kernel/releases/download/5.5.5/linux-headers-5.5.5_5.5.5-1-d9_amd64.deb
				
					dpkg -i linux-image-d9.deb
					dpkg -i linux-headers-d9.deb
				
					kernel_version="5.5.5"
				else
					echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
				fi
			elif [[ ${version} = "10" ]]; then
				if [[ ${bit} = "x86_64" ]]; then
					wget -N -O linux-image-d10.deb https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EY8ps95tSZhAkdbZeaEsMNsBbAbfHeUS0Hzc4Hk2Vr-QMQ?download=1
					wget -N -O linux-headers-d10.deb https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EVkOIxa9afBJm0ouo9XZzDkBVl10vSMMgrd58WUHTJdvfQ?download=1
				
					dpkg -i linux-image-d10.deb
					dpkg -i linux-headers-d10.deb
				
					kernel_version="5.5.10"
				else
					echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
				fi
			fi
		elif [[ "${release}" == "ubuntu" ]]; then
			if [[ ${version} = "16" ]]; then
				if [[ ${bit} = "x86_64" ]]; then
					wget -N -O linux-image-u16.deb https://github.com/ylx2016/kernel/releases/download/5.4.14/linux-image-5.4.14_5.4.14-1-u16_amd64.deb
					wget -N -O linux-headers-u16.deb https://github.com/ylx2016/kernel/releases/download/5.4.14/linux-headers-5.4.14_5.4.14-1-u16_amd64.deb
				
					dpkg -i linux-image-u16.deb
					dpkg -i linux-headers-u16.deb
				
					kernel_version="5.4.14"
				else
					echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
				fi
		
			elif [[ ${version} = "18" ]]; then
				if [[ ${bit} = "x86_64" ]]; then
					wget -N -O linux-image-u18.deb https://github.com/ylx2016/kernel/releases/download/5.4.14/linux-image-5.4.14_5.4.14-1-u18_amd64.deb
					wget -N -O linux-headers-u18.deb https://github.com/ylx2016/kernel/releases/download/5.4.14/linux-headers-5.4.14_5.4.14-1-u18_amd64.deb
				
					dpkg -i linux-image-u18.deb
					dpkg -i linux-headers-u18.deb
				
					kernel_version="5.4.14"
				else
					echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
				fi
			elif [[ ${version} = "19" ]]; then
				if [[ ${bit} = "x86_64" ]]; then
					wget -N -O linux-image-u19.deb https://github.com/ylx2016/kernel/releases/download/5.4.14/linux-headers-5.4.14_5.4.14-1-u19_amd64.deb
					wget -N -O linux-headers-u19.deb https://github.com/ylx2016/kernel/releases/download/5.4.14/linux-image-5.4.14_5.4.14-1-u19_amd64.deb
				
					dpkg -i linux-image-u19.deb
					dpkg -i linux-headers-u19.deb
				
					kernel_version="5.4.14"
				else
					echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
				fi
			fi				
			
		#else	
		#	wget http://security.debian.org/debian-security/pool/updates/main/o/openssl/libssl1.0.0_1.0.1t-1+deb8u10_amd64.deb
		#	wget -N --no-check-certificate http://${github}/bbr/debian-ubuntu/linux-headers-${kernel_version}-all.deb
		#	wget -N --no-check-certificate http://${github}/bbr/debian-ubuntu/${bit}/linux-headers-${kernel_version}.deb
		#	wget -N --no-check-certificate http://${github}/bbr/debian-ubuntu/${bit}/linux-image-${kernel_version}.deb
	
		#	dpkg -i libssl1.0.0_1.0.1t-1+deb8u10_amd64.deb
		#	dpkg -i linux-headers-${kernel_version}-all.deb
		#	dpkg -i linux-headers-${kernel_version}.deb
		#	dpkg -i linux-image-${kernel_version}.deb
		fi
	fi
	
	cd .. && rm -rf bbr	
	
	detele_kernel
	BBR_grub
	echo -e "${Tip} ${Red_font_prefix} check whether the above information about the kernel, the kernel no do not restart ${Font_color_suffix}"
	echo -e "${Tip} ${Red_font_prefix}rescue rescue is not a normal kernel, to exclude this ${Font_color_suffix}"
	echo -e "${Tip} After restarting the VPS, please re-run the script to enable ${Red_font_prefix}BBR${Font_color_suffix}"	
	stty erase '^H' && read -p "You need to restart the VPS before you can turn on BBR. Do you want to restart now? [Y/n]: " yn
	[ -z "${yn}" ] && yn="y"
	if [[ $yn == [Yy] ]]; then
		echo -e "${Info} VPS VPS is restarting... "
		reboot
	fi
	#echo -e "${Tip} kernel installation is complete, please refer to the above information to check whether the installation is successful and manually adjust the kernel boot sequence"
}

# Installation BBRplus kernel 4.14.129
installbbrplus(){
	kernel_version="4.14.160-bbrplus"
	bit=`uname -m`
	rm -rf bbrplus
	mkdir bbrplus && cd bbrplus
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O kernel-headers-c7.rpm https://github.com/cx9208/Linux-NetSpeed/raw/master/bbrplus/centos/7/kernel-headers-4.14.129-bbrplus.rpm
				wget -N -O kernel-c7.rpm https://github.com/cx9208/Linux-NetSpeed/raw/master/bbrplus/centos/7/kernel-4.14.129-bbrplus.rpm
				
				yum install -y kernel-c7.rpm
				yum install -y kernel-headers-c7.rpm
				
				kernel_version="4.14.129_bbrplus"
			else
					echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
			fi
		fi	
		
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		wget -N -O linux-headers.deb https://github.com/cx9208/Linux-NetSpeed/raw/master/bbrplus/debian-ubuntu/x64/linux-headers-4.14.129-bbrplus.deb
		wget -N -O linux-image.deb https://github.com/cx9208/Linux-NetSpeed/raw/master/bbrplus/debian-ubuntu/x64/linux-image-4.14.129-bbrplus.deb
		
		dpkg -i linux-image.deb
		dpkg -i linux-headers.deb
			
		kernel_version="4.14.129-bbrplus"
	fi
	
	cd .. && rm -rf bbrplus
	detele_kernel
	BBR_grub
	echo -e "${Tip} ${Red_font_prefix} check whether the above information about the kernel, the kernel no do not restart ${Font_color_suffix}"
	echo -e "${Tip} ${Red_font_prefix} rescue is not a normal kernel, to exclude this ${Font_color_suffix}"
	echo -e "${Tip} After restarting the VPS, please re-run the script to enable ${Red_font_prefix}BBRplus${Font_color_suffix}"
	stty erase '^H' && read -p "You need to restart the VPS before you can start BBRplus. Do you want to restart it now? [Y/n]: " yn
	[ -z "${yn}" ] && yn="y"
	if [[ $yn == [Yy] ]]; then
		echo -e "${Info} is restarting... "
		reboot
	fi
	#echo -e "${Tip} kernel installation is complete, please refer to the above information to check whether the installation is successful and manually adjust the kernel boot sequence"
}

# Install the kernel Lotserver
installlot(){
	if [[ "${release}" == "centos" ]]; then
		rpm --import http://${github}/lotserver/${release}/RPM-GPG-KEY-elrepo.org
		yum remove -y kernel-firmware
		yum install -y http://${github}/lotserver/${release}/${version}/${bit}/kernel-firmware-${kernel_version}.rpm
		yum install -y http://${github}/lotserver/${release}/${version}/${bit}/kernel-${kernel_version}.rpm
		yum remove -y kernel-headers
		yum install -y http://${github}/lotserver/${release}/${version}/${bit}/kernel-headers-${kernel_version}.rpm
		yum install -y http://${github}/lotserver/${release}/${version}/${bit}/kernel-devel-${kernel_version}.rpm
	elif [[ "${release}" == "ubuntu" ]]; then
		bash <(wget --no-check-certificate -qO- "http://${github}/Debian_Kernel.sh")
	elif [[ "${release}" == "debian" ]]; then
		bash <(wget --no-check-certificate -qO- "http://${github}/Debian_Kernel.sh")
	fi
	
	detele_kernel
	BBR_grub
	echo -e "${Tip} ${Red_font_prefix} check whether the above information about the kernel, the kernel no do not restart ${Font_color_suffix}"
	echo -e "${Tip} ${Red_font_prefix} rescue is not a normal kernel, to exclude this ${Font_color_suffix}"
	echo -e "${Tip After restarting the VPS, please re-run the script to enable ${Red_font_prefix}Lotserver${Font_color_suffix}"
	stty erase '^H' && read -p "You need to restart the VPS before you can start Lotserver. Do you want to restart it now? [Y/n]: " yn
	[ -z "${yn}" ] && yn="y"
	if [[ $yn == [Yy] ]]; then
		echo -e "${Info} VPS is restarting... "
		reboot
	fi
	#echo -e "${Tip} kernel installation is complete, please refer to the above information to check whether the installation is successful and manually adjust the kernel boot sequence"
}

# Install the kernel from xanmod.org xanmod
installxanmod(){
	kernel_version="5.5.1-xanmod1"
	bit=`uname -m`
	rm -rf xanmod
	mkdir xanmod && cd xanmod
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O kernel-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/ES1FqBSXMZlJlPgYrSagBCUBb6N2_srwS_e59B2Y74Rksg?download=1
				wget -N -O kernel-headers-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/ERUwIDLM1XdDt4xxv13xJOkBfpaQby-u2XdEr3d9I6ixag?download=1
				
				yum install -y kernel-c7.rpm
				yum install -y kernel-headers-c7.rpm
			
				kernel_version="5.5.8_xanmod6"
			else
				echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
			fi
		elif [[ ${version} = "8" ]]; then
				wget -N -O kernel-c8.rpm https://github.com/ylx2016/kernel/releases/download/5.5.1xanmod/kernel-5.5.1_xanmod1-1-c8.x86_64.rpm
				wget -N -O kernel-headers-c8.rpm https://github.com/ylx2016/kernel/releases/download/5.5.1xanmod/kernel-headers-5.5.1_xanmod1-1-c8.x86_64.rpm
				
				yum install -y kernel-c8.rpm
				yum install -y kernel-headers-c8.rpm
			
				kernel_version="5.5.1_xanmod1"
		fi
		
	elif [[ "${release}" == "debian" ]]; then
		if [[ ${version} = "9" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O linux-headers-d9.deb https://github.com/ylx2016/kernel/releases/download/5.5.1xanmod/linux-headers-5.5.1-xanmod1_5.5.1-xanmod1-1-d9_amd64.deb
				wget -N -O linux-image-d9.deb https://github.com/ylx2016/kernel/releases/download/5.5.1xanmod/linux-image-5.5.1-xanmod1_5.5.1-xanmod1-1-d9_amd64.deb
				
				dpkg -i linux-image-d9.deb
				dpkg -i linux-headers-d9.deb
				
				#kernel_version="4.14.168-bbrplus"
			else
				echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
			fi	
		elif [[ ${version} = "10" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O linux-headers-d10.deb https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EcwO-0rZ3VlFjBNHYoVxy_IBgn2E_viwNx0l2fGHtRwyJw?download=1
				wget -N -O linux-image-d10.deb https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EcJBiKjxLp5Lq0tkTUlXcfEBH2wRr_n83jV4aVvii3q2Fg?download=1
					
				dpkg -i linux-image-d10.deb
				dpkg -i linux-headers-d10.deb
				
				kernel_version="5.5.8-xanmod6"
			else
				echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
			fi		
		fi			
	fi
	
	cd .. && rm -rf xanmod
	detele_kernel
	BBR_grub
	echo -e "${Tip} ${Red_font_prefix} check whether the above information about the kernel, the kernel no do not restart ${Font_color_suffix}"
	echo -e "${Tip} ${Red_font_prefix} rescue is not a normal kernel, to exclude this ${Font_color_suffix}"
	echo -e "${Tip} After restarting the VPS, please re-run the script to enable ${Red_font_prefix}BBR${Font_color_suffix}"
	stty erase '^H' && read -p "You need to restart the VPS before you can turn on BBR. Do you want to restart now? [Y/n]: " yn
	[ -z "${yn}" ] && yn="y"
	if [[ $yn == [Yy] ]]; then
		echo -e "${Info} VPS is restarting... "
		reboot
	fi
	#echo -e "${Tip} kernel installation is complete, please refer to the above information to check whether the installation is successful and manually adjust the kernel boot sequence"
}

# Install the kernel bbr2
installbbr2(){
	kernel_version="5.4.0-rc6"
	bit=`uname -m`
	rm -rf bbr2
	mkdir bbr2 && cd bbr2
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O kernel-c7.rpm https://github.com/ylx2016/kernel/releases/download/5.4.0r6bbr2/kernel-5.4.0_rc6-1-bbr2-c7.x86_64.rpm
				wget -N -O kernel-headers-c7.rpm https://github.com/ylx2016/kernel/releases/download/5.4.0r6bbr2/kernel-headers-5.4.0_rc6-1-bbr2-c7.x86_64.rpm
				
				yum install -y kernel-c7.rpm
				yum install -y kernel-headers-c7.rpm
			
				kernel_version="5.4.0_rc6"
			else
				echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
			fi
		elif [[ ${version} = "8" ]]; then
				wget -N -O kernel-c8.rpm https://github.com/ylx2016/kernel/releases/download/5.4.0r6bbr2/kernel-5.4.0_rc6-1-bbr2-c8.x86_64.rpm
				wget -N -O kernel-headers-c8.rpm https://github.com/ylx2016/kernel/releases/download/5.4.0r6bbr2/kernel-headers-5.4.0_rc6-1-bbr2-c8.x86_64.rpm
				
				yum install -y kernel-c8.rpm
				yum install -y kernel-headers-c8.rpm
			
				kernel_version="5.4.0_rc6"
		fi
		
	elif [[ "${release}" == "debian" ]]; then
		if [[ ${version} = "9" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O linux-image-d9.deb https://github.com/ylx2016/kernel/releases/download/5.4.0r6bbr2/linux-image-5.4.0-rc6_5.4.0-rc6-1-bbr2-d9_amd64.deb
				wget -N -O linux-headers-d9.deb https://github.com/ylx2016/kernel/releases/download/5.4.0r6bbr2/linux-headers-5.4.0-rc6_5.4.0-rc6-1-bbr2-d9_amd64.deb
				
				dpkg -i linux-image-d9.deb
				dpkg -i linux-headers-d9.deb
				
				#kernel_version="4.14.168-bbrplus"
			else
				echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
			fi	
		elif [[ ${version} = "10" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O linux-headers-d10.deb https://github.com/ylx2016/kernel/releases/download/5.4.0r6bbr2/linux-headers-5.4.0-rc6_5.4.0-rc6-1-bbr2-d10_amd64.deb
				wget -N -O linux-image-d10.deb https://github.com/ylx2016/kernel/releases/download/5.4.0r6bbr2/linux-image-5.4.0-rc6_5.4.0-rc6-1-bbr2-d10_amd64.deb
					
				dpkg -i linux-image-d10.deb
				dpkg -i linux-headers-d10.deb
				
				#kernel_version="4.14.168-bbrplus"
			else
				echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
			fi		
		fi			
	fi
	
	cd .. && rm -rf bbr2
	detele_kernel
	BBR_grub
	echo -e "${Tip} ${Red_font_prefix} check whether the above information about the kernel, the kernel no do not restart ${Font_color_suffix}"
	echo -e "${Tip} ${Red_font_prefix} rescue is not a normal kernel, to exclude this v{Font_color_suffix}"
	echo -e "${Tip} After restarting the VPS, please re-run the script to enable ${Red_font_prefix}BBR2${Font_color_suffix}"
	stty erase '^H' && read -p "You need to restart the VPS before you can turn on BBR2. Do you want to restart it now? [Y/n]: " yn
	[ -z "${yn}" ] && yn="y"
	if [[ $yn == [Yy] ]]; then
		echo -e "${Info} VPS Reiniciando..."
		reboot
	fi
	# echo -e "${Tip} kernel installation is complete, please refer to the above information to check whether the installation is successful and manually adjust the kernel boot sequence"
}

# Install the kernel Zen
installzen(){
	kernel_version="5.5.2-zen"
	bit=`uname -m`
	rm -rf zen
	mkdir zen && cd zen
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O kernel-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EfQb4N8c2bxDlF3mj3SBVHIBGFSg_d1uR4LFzzT0Ii5FWA?download=1
				wget -N -O kernel-headers-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EfKgMa8vsZBOt0zwXM_lHcUBOYlyH1CyRHrYSRJ5r6a0EQ?download=1
				
				yum install -y kernel-c7.rpm
				yum install -y kernel-headers-c7.rpm
			
				kernel_version="5.5.10_zen"
			else
				echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
			fi
		elif [[ ${version} = "8" ]]; then
				wget -N -O kernel-c8.rpm https://github.com/ylx2016/kernel/releases/download/5.5.2zen/kernel-5.5.2_zen-1-c8.x86_64.rpm
				wget -N -O kernel-headers-c8.rpm https://github.com/ylx2016/kernel/releases/download/5.5.2zen/kernel-headers-5.5.2_zen-1-c8.x86_64.rpm
				
				yum install -y kernel-c8.rpm
				yum install -y kernel-headers-c8.rpm
			
				kernel_version="5.5.2_zen"
		fi
		
	elif [[ "${release}" == "debian" ]]; then
		if [[ ${version} = "9" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O linux-headers-d9.deb https://github.com/ylx2016/kernel/releases/download/5.5.2zen/linux-headers-5.5.2-zen_5.5.2-zen-1-d9_amd64.deb
				wget -N -O linux-image-d9.deb https://github.com/ylx2016/kernel/releases/download/5.5.2zen/linux-image-5.5.2-zen_5.5.2-zen-1-d9_amd64.deb
				
				dpkg -i linux-image-d9.deb 
				dpkg -i linux-headers-d9.deb
				
				#kernel_version="4.14.168-bbrplus"
			else
				echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
			fi	
		elif [[ ${version} = "10" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O linux-headers-d10.deb https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EShzFq8Jlv1PthbYlNNvLjIB2-hktrkPXxwd9mqcXgmcyg?download=1
				wget -N -O linux-image-d10.deb https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/ERXzOc-2BzJInOxBgKo62OkBgcI9-O-fw0M8U2B4NazuLg?download=1
					
				dpkg -i linux-image-d10.deb
				dpkg -i linux-headers-d10.deb
				
				kernel_version="5.5.10-zen"
			else
				echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
			fi		
		fi			
	fi
	
	cd .. && rm -rf zen
	detele_kernel
	BBR_grub
	echo -e "${Tip} ${Red_font_prefix} check whether the above information about the kernel, the kernel no do not restart ${Font_color_suffix}"
	echo -e "${Tip} ${Red_font_prefix} rescue is not a normal kernel, to exclude this ${Font_color_suffix}"
	echo -e "${Tip} After restarting the VPS, please re-run the script to enable ${Red_font_prefix}BBR${Font_color_suffix}"
	stty erase '^H' && read -p "You need to restart the VPS before you can turn on BBR. Do you want to restart now? [Y/n]: " yn
	[ -z "${yn}" ] && yn="y"
	if [[ $yn == [Yy] ]]; then
		echo -e "${Info} VPS is restarting... "
		reboot
	fi
	#echo -e "${Tip} kernel installation is complete, please refer to the above information to check whether the installation is successful and manually adjust the kernel boot sequence"
}

# Install the new kernel bbrplus
installbbrplusnew(){
	kernel_version="4.14.173-bbrplus"
	bit=`uname -m`
	rm -rf bbrplusnew
	mkdir bbrplusnew && cd bbrplusnew
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O kernel-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EWu6fCx32KxEvBrWqe5pZbAB6Y13ogTMfMfPnQWzfQpmiQ?download=1
				wget -N -O kernel-headers-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EZVCeWQA8rdMrNMysFO2V_0BJWB6Mlrc-IzLD_Xni4HuTQ?download=1
				
				yum install -y kernel-c7.rpm
				yum install -y kernel-headers-c7.rpm
			
				kernel_version="4.14.173_bbrplus"
			else
				echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
			fi
		fi
	elif [[ "${release}" == "debian" ]]; then
		if [[ ${version} = "10" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O linux-headers-d10.deb https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/ERSKfg1XkUJImM9fWZ3N8WwB3ygLGBzAT3-2Qf37UOokcw?download=1
				wget -N -O linux-image-d10.deb https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EeSISwYaxadNr81olVZh_usBHwvnt0J6W__-4nV-AKY9HQ?download=1
					
				dpkg -i linux-image-d10.deb
				dpkg -i linux-headers-d10.deb
				
				kernel_version="4.14.173-bbrplus"
			else
				echo -e "${Error} is still using a 32-bit kernel, don’t see you again! " && exit 1
			fi		
		fi			
	fi

	cd .. && rm -rf bbrplusnew
	detele_kernel
	BBR_grub
	echo -e "${Tip} ${Red_font_prefix} check whether the above information about the kernel, the kernel no do not restart ${Font_color_suffix}"
	echo -e "${Tip} ${Red_font_prefix} rescue is not a normal kernel, to exclude this ${Font_color_suffix}"
	echo -e "${Tip} After restarting the VPS, please re-run the script to enable ${Red_font_prefix} BBRplus ${Red_font_prefix}BBRplus${Font_color_suffix}"
	stty erase '^H' && read -p "You need to restart the VPS before you can start BBRplus. Do you want to restart it now? [Y/n]: " yn
	[ -z "${yn}" ] && yn="y"
	if [[ $yn == [Yy] ]]; then
		echo -e "${Info} VPS is restarting... "
		reboot
	fi
	# echo -e "${Tip} kernel installation is complete, please refer to the above information to check whether the installation is successful and manually adjust the kernel boot sequence"

}

# Enable BBR + fq
startbbrfq(){
	remove_all
	echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBR+FQ started successfully! "
}

#Enable BBR + cake
startbbrcake(){
	remove_all
	echo "net.core.default_qdisc=cake" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBR+cake started successfully! "
}

#Enable BBRplus
startbbrplus(){
	remove_all
	echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbrplus" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBRplus started successfully! "
}

#Enable Lotserver
startlotserver(){
	remove_all
	if [[ "${release}" == "centos" ]]; then
		yum install ethtool
	else
		apt-get update
		apt-get install ethtool
	fi
	bash <(wget --no-check-certificate -qO- https://raw.githubusercontent.com/chiakge/lotServer/master/Install.sh) install
	sed -i '/advinacc/d' /appex/etc/config
	sed -i '/maxmode/d' /appex/etc/config
	echo -e "advinacc=\"1\"
maxmode=\"1\"">>/appex/etc/config
	/appex/bin/lotServer.sh restart
	start_menu
}

# Enable BBR2 + FQ
startbbr2fq(){
	remove_all
	echo "net.ipv4.tcp_ecn=0" >> /etc/sysctl.conf
	echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbr2" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBR2 started successfully! "
}

# Enable BBR2+CAKE
startbbr2cake(){
	remove_all
	echo "net.ipv4.tcp_ecn=0" >> /etc/sysctl.conf
	echo "net.core.default_qdisc=cake" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbr2" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBR2 started successfully! "
}

# Enable BBR2+FQ+ecn
startbbr2fqecn(){
	remove_all
	echo "net.ipv4.tcp_ecn=1" >> /etc/sysctl.conf
	echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbr2" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBR2 started successfully! "
}

# Enable BBR2+CAKE+ecn
startbbr2cakeecn(){
	remove_all
	echo "net.ipv4.tcp_ecn=1" >> /etc/sysctl.conf
	echo "net.core.default_qdisc=cake" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbr2" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBR2 started successfully! "
}


# Uninstall all accelerated
remove_all(){
	rm -rf bbrmod
	sed -i '/net.ipv4.tcp_retries2/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_slow_start_after_idle/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_fastopen/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_ecn/d' /etc/sysctl.conf
	sed -i '/net.core.default_qdisc/d' /etc/sysctl.conf
    sed -i '/net.ipv4.tcp_congestion_control/d' /etc/sysctl.conf
    sed -i '/fs.file-max/d' /etc/sysctl.conf
	sed -i '/net.core.rmem_max/d' /etc/sysctl.conf
	sed -i '/net.core.wmem_max/d' /etc/sysctl.conf
	sed -i '/net.core.rmem_default/d' /etc/sysctl.conf
	sed -i '/net.core.wmem_default/d' /etc/sysctl.conf
	sed -i '/net.core.netdev_max_backlog/d' /etc/sysctl.conf
	sed -i '/net.core.somaxconn/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_syncookies/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_tw_reuse/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_tw_recycle/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_fin_timeout/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_keepalive_time/d' /etc/sysctl.conf
	sed -i '/net.ipv4.ip_local_port_range/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_max_syn_backlog/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_max_tw_buckets/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_rmem/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_wmem/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_mtu_probing/d' /etc/sysctl.conf
	sed -i '/net.ipv4.ip_forward/d' /etc/sysctl.conf
	sed -i '/fs.inotify.max_user_instances/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_syncookies/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_fin_timeout/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_tw_reuse/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_max_syn_backlog/d' /etc/sysctl.conf
	sed -i '/net.ipv4.ip_local_port_range/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_max_tw_buckets/d' /etc/sysctl.conf
	sed -i '/net.ipv4.route.gc_timeout/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_synack_retries/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_syn_retries/d' /etc/sysctl.conf
	sed -i '/net.core.somaxconn/d' /etc/sysctl.conf
	sed -i '/net.core.netdev_max_backlog/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_timestamps/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_max_orphans/d' /etc/sysctl.conf
	if [[ -e /appex/bin/lotServer.sh ]]; then
		bash <(wget --no-check-certificate -qO- https://github.com/MoeClub/lotServer/raw/master/Install.sh) uninstall
	fi
	clear
	echo -e "${Info} : Clear and accelerate completed. "
	sleep 1s
}

# Optimization System Configuration
optimizing_system(){
	sed -i '/net.ipv4.tcp_retries2/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_slow_start_after_idle/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_fastopen/d' /etc/sysctl.conf
	sed -i '/fs.file-max/d' /etc/sysctl.conf
	sed -i '/fs.inotify.max_user_instances/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_syncookies/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_fin_timeout/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_tw_reuse/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_max_syn_backlog/d' /etc/sysctl.conf
	sed -i '/net.ipv4.ip_local_port_range/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_max_tw_buckets/d' /etc/sysctl.conf
	sed -i '/net.ipv4.route.gc_timeout/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_synack_retries/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_syn_retries/d' /etc/sysctl.conf
	sed -i '/net.core.somaxconn/d' /etc/sysctl.conf
	sed -i '/net.core.netdev_max_backlog/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_timestamps/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_max_orphans/d' /etc/sysctl.conf
	sed -i '/net.ipv4.ip_forward/d' /etc/sysctl.conf
	echo "net.ipv4.tcp_retries2 = 8
net.ipv4.tcp_slow_start_after_idle = 0
net.ipv4.tcp_fastopen = 3
fs.file-max = 1000000
fs.inotify.max_user_instances = 8192
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_fin_timeout = 30
net.ipv4.tcp_tw_reuse = 1
net.ipv4.ip_local_port_range = 1024 65000
net.ipv4.tcp_max_syn_backlog = 16384
net.ipv4.tcp_max_tw_buckets = 6000
net.ipv4.route.gc_timeout = 100
net.ipv4.tcp_syn_retries = 1
net.ipv4.tcp_synack_retries = 1
net.core.somaxconn = 32768
net.core.netdev_max_backlog = 32768
net.ipv4.tcp_timestamps = 0
net.ipv4.tcp_max_orphans = 32768
# forward ipv4
net.ipv4.ip_forward = 1">>/etc/sysctl.conf
	sysctl -p
	echo "*               soft    nofile           1000000
*               hard    nofile          1000000">/etc/security/limits.conf
	echo "ulimit -SHn 1000000">>/etc/profile
	read -p "You need to restart the VPS for the system optimization configuration to take effect. Do you want to restart now? [Y/n]: " yn
	[ -z "${yn}" ] && yn="y"
	if [[ $yn == [Yy] ]]; then
		echo -e "${Info} VPS is restarting... "
		reboot
	fi
}
# Update script
Update_Shell(){
	echo -e "The current version is [ ${sh_ver} ], start checking the latest version... "
	sh_new_ver=$(wget --no-check-certificate -qO- "https://${github}/tcp.sh"|grep 'sh_ver="'|awk -F "=" '{print $NF}'|sed 's/\"//g'|head -1)
	[[ -z ${sh_new_ver} ]] && echo -e "${Error} failed to detect the latest version! " && start_menu
	if [[ ${sh_new_ver} != ${sh_ver} ]]; then
		echo -e " Found a new version [ ${sh_new_ver} ], do you want to update it? [Y/n] "
		read -p "(Opc: y):" yn
		[[ -z "${yn}" ]] && yn="y"
		if [[ ${yn} == [Yy] ]]; then
			wget -N --no-check-certificate http://${github}/tcp.sh && chmod +x tcp.sh
			echo -e "The script has been updated to the latest version [ ${sh_new_ver} ] !"
		else
			echo && echo " 	Cancelled... " && echo
		fi
	else
		echo -e " Currently is the latest version [ ${sh_new_ver} ] !"
		sleep 5s
	fi
}

# Switch to kernel version without uninstalling
gototcpx(){
	clear
	wget -N --no-check-certificate "https://github.000060000.xyz/tcpx.sh" && chmod +x tcpx.sh && ./tcpx.sh
}

#Start Menu
start_menu(){
clear
echo && echo -e " TCP accelerated one-key installation management script ${Red_font_prefix}[v${sh_ver}]${Font_color_suffix}
 Update content and feedback:  https://blog.ylx.me/archives/783.html
  
 ${Green_font_prefix}0.${Font_color_suffix} upgrade script
 ${Green_font_prefix}9.${Font_color_suffix} switch to not uninstall the kernel version
————————————Kernel Management————————————
 ${Green_font_prefix}1.${Font_color_suffix} install the original BBR kernel-5.4.14/5.5.5/5.5.10
 ${Green_font_prefix}2.${Font_color_suffix} install the BBRplus version of the kernel-4.14.129
 ${Green_font_prefix}3.${Font_color_suffix} install Lotserver kernel-various
 ${Green_font_prefix}4.${Font_color_suffix} install xanmod version kernel-5.5.1/5.5.8
 ${Green_font_prefix}5.${Font_color_suffix} install BBR2 beta kernel-5.4.0
 ${Green_font_prefix}6.${Font_color_suffix} install the Zen version of the kernel-5.5.2/5.5.10
 ${Green_font_prefix}7.${Font_color_suffix} install the new version of BBRplus kernel-4.14.173
————————————Accelerated Management————————————
 ${Green_font_prefix}11.${Font_color_suffix} uses BBR+FQ to accelerate
 ${Green_font_prefix}12.${Font_color_suffix} uses BBR+CAKE to accelerate
 ${Green_font_prefix}13.${Font_color_suffix} uses BBRplus+FQ version to accelerate
 ${Green_font_prefix}14.${Font_color_suffix} uses Lotserver (sharp speed) to accelerate
 ${Green_font_prefix}15.${Font_color_suffix} uses BBR2+FQ to accelerate
 ${Green_font_prefix}16.${Font_color_suffix} uses BBR2+CAKE to accelerate
 ${Green_font_prefix}17.${Font_color_suffix} uses BBR2+FQ+ECN to accelerate
 ${Green_font_prefix}18.${Font_color_suffix} uses BBR2+CAKE+ECN to accelerate
————————————Miscellaneous Management————————————
 ${Green_font_prefix}21.${Font_color_suffix} Uninstall all speed up
 ${Green_font_prefix}22.${Font_color_suffix} system configuration optimization
 ${Green_font_prefix}23.${Font_color_suffix} exit script
————————————————————————————————" && echo

	check_status
	echo -e "The current kernel is: ${Font_color_suffix}${kernel_version_r}${Font_color_suffix}"
	if [[ ${kernel_status} == "noinstall" ]]; then
		echo -e " Current status: ${Green_font_prefix}No instalado${Font_color_suffix} Acelera el kernel ${Red_font_prefix}Primero instale el kernel${Font_color_suffix}"
	else
		echo -e " Current status: ${Green_font_prefix}Ha sido instalado${Font_color_suffix} ${_font_prefix}${kernel_status}${Font_color_suffix} Acelera el kernel, ${Green_font_prefix}${run_status}${Font_color_suffix}"
		
	fi
	echo -e "The current congestion control algorithm is: ${Green_font_prefix}${net_congestion_control}${Font_color_suffix} El algoritmo de cola actual es: ${Green_font_prefix}${net_qdisc}${Font_color_suffix} "
	
echo
read -p " Please enter a number: " num
case "$num" in
	0)
	Update_Shell
	;;
	1)
	check_sys_bbr
	;;
	2)
	check_sys_bbrplus
	;;
	3)
	check_sys_Lotsever
	;;
	4)
	check_sys_xanmod
	;;
	5)
	check_sys_bbr2
	;;
	6)
	check_sys_zen
	;;
	7)
	check_sys_bbrplusnew	
	;;
	9)
	gototcpx
	;;
	11)
	startbbrfq
	;;
	12)
	startbbrcake
	;;
	13)
	startbbrplus
	;;
	14)
	startlotserver
	;;
	15)
	startbbr2fq
	;;
	16)
	startbbr2cake
	;;
	17)
	startbbr2fqecn
	;;
	18)
	startbbr2cakeecn
	;;
	21)
	remove_all
	;;
	22)
	optimizing_system
	;;
	23)
	exit 1
	;;
	*)
	clear
	echo -e "${Error}: Please enter the correct number [0-11] "
	sleep 5s
	start_menu
	;;
esac
}
############Core Management Components#############

# Remove the extra cores
detele_kernel(){
	if [[ "${release}" == "centos" ]]; then
		rpm_total=`rpm -qa | grep kernel | grep -v "${kernel_version}" | grep -v "noarch" | wc -l`
		if [ "${rpm_total}" > "1" ]; then
			echo -e " Uninstall ${rpm_total} remaining cores detected , start uninstalling... "
			for((integer = 1; integer <= ${rpm_total}; integer++)); do
				rpm_del=`rpm -qa | grep kernel | grep -v "${kernel_version}" | grep -v "noarch" | head -${integer}`
				echo -e " Start to uninstall the ${rpm_del} kernel... "
				rpm --nodeps -e ${rpm_del}
				echo -e " Uninstall ${rpm_del} kernel uninstall complete, continue... "
			done
			echo --nodeps -e "The kernel is uninstalled, continue... "
		else
			echo -e "The number of cores detected is incorrect, please check! " && exit 1
		fi
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		deb_total=`dpkg -l | grep linux-image | awk '{print $2}' | grep -v "${kernel_version}" | wc -l`
		if [ "${deb_total}" > "1" ]; then
			echo -e " Uninstall ${deb_total} remaining cores detected , start uninstalling... "
			for((integer = 1; integer <= ${deb_total}; integer++)); do
				deb_del=`dpkg -l|grep linux-image | awk '{print $2}' | grep -v "${kernel_version}" | head -${integer}`
				echo -e " Start to uninstall the ${deb_del} kernel... "
				apt-get purge -y ${deb_del}
				echo -e " Uninstall ${deb_del} kernel uninstall complete, continue... "
			done
			echo -e "The kernel is uninstalled, continue... "
		else
			echo -e "The number of cores detected is incorrect, please check! " && exit 1
		fi
	fi
}

# Update the boot
BBR_grub(){
	if [[ "${release}" == "centos" ]]; then
        if [[ ${version} = "6" ]]; then
            if [ ! -f "/boot/grub/grub.conf" ]; then
                echo -e "${Error} /boot/grub/grub.conf not found, please check. "
                exit 1
            fi
            sed -i 's/^default=.*/default=0/g' /boot/grub/grub.conf
        elif [[ ${version} = "7" ]]; then
            if [ -f "/boot/grub2/grub.cfg" ]; then
				grub2-mkconfig  -o   /boot/grub2/grub.cfg
				grub2-set-default 0
				exit 1
			elif [ -f "/boot/efi/EFI/centos/grub.cfg" ]; then
				grub2-mkconfig  -o   /boot/efi/EFI/centos/grub.cfg
				grub2-set-default 0
				exit 1
			else
				echo -e "${Error} grub.cfg cannot be found, please check. "
            fi
			#grub2-mkconfig  -o   /boot/grub2/grub.cfg
			#grub2-set-default 0
		
		elif [[ ${version} = "8" ]]; then
			grub2-mkconfig  -o   /boot/grub2/grub.cfg
			grubby --info=ALL|awk -F= '$1=="kernel" {print i++ " : " $2}'
        fi
    elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
        /usr/sbin/update-grub
    fi
}

# ###########Core Management Components#############



# ###########System Testing Components#############

# Inspection system
check_sys(){
	if [[ -f /etc/redhat-release ]]; then
		release="centos"
	elif cat /etc/issue | grep -q -E -i "debian"; then
		release="debian"
	elif cat /etc/issue | grep -q -E -i "ubuntu"; then
		release="ubuntu"
	elif cat /etc/issue | grep -q -E -i "centos|red hat|redhat"; then
		release="centos"
	elif cat /proc/version | grep -q -E -i "debian"; then
		release="debian"
	elif cat /proc/version | grep -q -E -i "ubuntu"; then
		release="ubuntu"
	elif cat /proc/version | grep -q -E -i "centos|red hat|redhat"; then
		release="centos"
    fi
	
# Processing ca certificate
	if [[ "${release}" == "centos" ]]; then
		yum install ca-certificates -y
		update-ca-trust force-enable
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		apt-get install ca-certificates -y
		update-ca-certificates
	fi	
}

# Check the Linux version
check_version(){
	if [[ -s /etc/redhat-release ]]; then
		version=`grep -oE  "[0-9.]+" /etc/redhat-release | cut -d . -f 1`
	else
		version=`grep -oE  "[0-9.]+" /etc/issue | cut -d . -f 1`
	fi
	bit=`uname -m`
	if [[ ${bit} = "x86_64" ]]; then
		bit="x64"
	else
		bit="x32"
	fi
}

# Check the system requirements for installing bbr
check_sys_bbr(){
	check_version
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "6" || ${version} = "7" || ${version} = "8" ]]; then
			installbbr
		else
			echo -e "${Error} BBR kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	elif [[ "${release}" == "debian" ]]; then
		if [[ ${version} = "8" || ${version} = "9" || ${version} = "10" ]]; then
			installbbr
		else
			echo -e "${Error} BBR kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	elif [[ "${release}" == "ubuntu" ]]; then
		if [[ ${version} = "16" || ${version} = "18" || ${version} = "19" ]]; then
			installbbr
		else
			echo -e "${Error} BBR BBR kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	else
		echo -e "${Error} BBR kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	fi
}

check_sys_bbrplus(){
	check_version
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" ]]; then
			installbbrplus
		else
			echo -e "${Error} BBRplus kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	elif [[ "${release}" == "debian" ]]; then
		if [[ ${version} = "8" || ${version} = "9" || ${version} = "10" ]]; then
			installbbrplus
		else
			echo -e "${Error} BBRplus kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	elif [[ "${release}" == "ubuntu" ]]; then
		if [[ ${version} = "16" || ${version} = "18" || ${version} = "19" ]]; then
			installbbrplus
		else
			echo -e "${Error} BBRplus kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	else
		echo -e "${Error} BBRplus kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	fi
}

check_sys_bbrplusnew(){
	check_version
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" ]]; then
			installbbrplusnew
		else
			echo -e "${Error} BBRplusNew kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	elif [[ "${release}" == "debian" ]]; then
		if [[ ${version} = "10" ]]; then
			installbbrplusnew
		else
			echo -e "${Error} BBRplusNew kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	else
		echo -e "${Error} BBRplusNew kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	fi
}

check_sys_xanmod(){
	check_version
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" || ${version} = "8" ]]; then
			installxanmod
		else
			echo -e "${Error} xanmod kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	elif [[ "${release}" == "debian" ]]; then
		if [[ ${version} = "9" || ${version} = "10" ]]; then
			installxanmod
		else
			echo -e "${Error} xanmod kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	elif [[ "${release}" == "ubuntu" ]]; then
			echo -e "${Error} xanmod kernel does not support the current system ${release} ${version} ${bit} ,去xanmod.org 官网安装吧!" && exit 1
	else
		echo -e "${Error} xanmod kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	fi
}

check_sys_bbr2(){
	check_version
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" || ${version} = "8" ]]; then
			installbbr2
		else
			echo -e "${Error} bbr2 kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	elif [[ "${release}" == "debian" ]]; then
		if [[ ${version} = "9" || ${version} = "10" ]]; then
			installbbr2
		else
			echo -e "${Error} bbr2 kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	elif [[ "${release}" == "ubuntu" ]]; then
			echo -e "${Error} bbr2 kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	else
		echo -e "${Error} bbr2 kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	fi
}


check_sys_zen(){
	check_version
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" || ${version} = "8" ]]; then
			installzen
		else
			echo -e "${Error} zen kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	elif [[ "${release}" == "debian" ]]; then
		if [[ ${version} = "9" || ${version} = "10" ]]; then
			installzen
		else
			echo -e "${Error} zen kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	elif [[ "${release}" == "ubuntu" ]]; then
			echo -e "${Error} zen kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	else
		echo -e "${Error} zen kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	fi
}

# Check the system requirements for installing Lotsever
check_sys_Lotsever(){
	check_version
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} == "6" ]]; then
			kernel_version="2.6.32-504"
			installlot
		elif [[ ${version} == "7" ]]; then
			yum -y install net-tools
			kernel_version="4.11.2-1"
			installlot
		else
			echo -e "${Error} Lotsever does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	elif [[ "${release}" == "debian" ]]; then
		if [[ ${version} = "7" || ${version} = "8" ]]; then
			if [[ ${bit} == "x64" ]]; then
				kernel_version="3.16.0-4"
				installlot
			elif [[ ${bit} == "x32" ]]; then
				kernel_version="3.2.0-4"
				installlot
			fi
		elif [[ ${version} = "9" ]]; then
			if [[ ${bit} == "x64" ]]; then
				kernel_version="4.9.0-4"
				installlot
			fi
		else
			echo -e "${Error} Lotsever does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	elif [[ "${release}" == "ubuntu" ]]; then
		if [[ ${version} -ge "12" ]]; then
			if [[ ${bit} == "x64" ]]; then
				kernel_version="4.4.0-47"
				installlot
			elif [[ ${bit} == "x32" ]]; then
				kernel_version="3.13.0-29"
				installlot
			fi
		else
			echo -e "${Error} Lotsever does not support the current system ${release} ${version} ${bit} !" && exit 1
		fi
	else
		echo -e "${Error} Lotsever does not support the current system ${release} ${version} ${bit} !" && exit 1
	fi
}

check_status(){
	kernel_version=`uname -r | awk -F "-" '{print $1}'`
	kernel_version_full=`uname -r`
	net_congestion_control=`cat /proc/sys/net/ipv4/tcp_congestion_control | awk '{print $1}'`
	net_qdisc=`cat /proc/sys/net/core/default_qdisc | awk '{print $1}'`
	kernel_version_r=`uname -r | awk '{print $1}'`
	if [[ ${kernel_version_full} = "4.14.168-bbrplus" || ${kernel_version_full} = "4.14.98-bbrplus" || ${kernel_version_full} = "4.14.129-bbrplus" || ${kernel_version_full} = "4.14.160-bbrplus" || ${kernel_version_full} = "4.14.166-bbrplus" || ${kernel_version_full} = "4.14.161-bbrplus" ]]; then
		kernel_status="BBRplus"
	elif [[ ${kernel_version} = "3.10.0" || ${kernel_version} = "3.16.0" || ${kernel_version} = "3.2.0" || ${kernel_version} = "4.4.0" || ${kernel_version} = "3.13.0"  || ${kernel_version} = "2.6.32" || ${kernel_version} = "4.9.0" || ${kernel_version} = "4.11.2" ]]; then
		kernel_status="Lotserver"
	elif [[ `echo ${kernel_version} | awk -F'.' '{print $1}'` == "4" ]] && [[ `echo ${kernel_version} | awk -F'.' '{print $2}'` -ge 9 ]] || [[ `echo ${kernel_version} | awk -F'.' '{print $1}'` == "5" ]]; then
		kernel_status="BBR"
	else 
		kernel_status="noinstall"
	fi
	

	if [[ ${kernel_status} == "BBR" ]]; then
		run_status=`cat /proc/sys/net/ipv4/tcp_congestion_control | awk '{print $1}'`
		if [[ ${run_status} == "bbr" ]]; then
			run_status=`cat /proc/sys/net/ipv4/tcp_congestion_control | awk '{print $1}'`
			if [[ ${run_status} == "bbr" ]]; then
				run_status=" BBR started successfully "
			else 
				run_status=" BBR failed to start "
			fi
		elif [[ ${run_status} == "bbr2" ]]; then
			run_status=`cat /proc/sys/net/ipv4/tcp_congestion_control | awk '{print $1}'`
			if [[ ${run_status} == "bbr2" ]]; then
				run_status=" BBR2 started successfully "
			else 
				run_status=" BBR2 failed to start "
			fi	
		elif [[ ${run_status} == "tsunami" ]]; then
			run_status=`lsmod | grep "tsunami" | awk '{print $1}'`
			if [[ ${run_status} == "tcp_tsunami" ]]; then
				run_status=" BBR Magic Revamp started successfully "
			else 
				run_status=" BBR Magic Revamp failed to start "
			fi
		elif [[ ${run_status} == "nanqinlang" ]]; then
			run_status=`lsmod | grep "nanqinlang" | awk '{print $1}'`
			if [[ ${run_status} == "tcp_nanqinlang" ]]; then
				run_status="The violent BBR magic revision started successfully "
			else 
				run_status="The violent BBR magic revision failed to start "
			fi
		else 
			run_status=" Acceleration module not installed "
		fi
		
	elif [[ ${kernel_status} == "Lotserver" ]]; then
		if [[ -e /appex/bin/lotServer.sh ]]; then
			run_status=`bash /appex/bin/lotServer.sh status | grep "LotServer" | awk  '{print $3}'`
			if [[ ${run_status} = "running!" ]]; then
				run_status=" Started successfully "
			else 
				run_status=" Start failed "
			fi
		else 
			run_status=" Acceleration module not installed "
		fi	
	elif [[ ${kernel_status} == "BBRplus" ]]; then
		run_status=`cat /proc/sys/net/ipv4/tcp_congestion_control | awk '{print $1}'`
		if [[ ${run_status} == "bbrplus" ]]; then
			run_status=`cat /proc/sys/net/ipv4/tcp_congestion_control | awk '{print $1}'`
			if [[ ${run_status} == "bbrplus" ]]; then
				run_status=" BBRplus started successfully "
			else 
				run_status=" BBRplus failed to start "
			fi
		else 
			run_status=" Acceleration module not installed "
		fi
	fi
}

#Componentes de detección del sistema#
check_sys
check_version
[[ ${release} != "debian" ]] && [[ ${release} != "ubuntu" ]] && [[ ${release} != "centos" ]] && echo -e "${Error} 本脚本不支持当前系统 ${release} !" && exit 1
start_menu
