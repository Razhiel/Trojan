#!/bin/bash
#
#Author: kenzo
#
#
function blue(){
    echo -e "\033[34m\033[01m$1\033[0m"
}
function green(){
    echo -e "\033[32m\033[01m$1\033[0m"
}
function cyan(){
    echo -e "\033[36m\033[01m$1\033[0m"
}
function version_lt(){
    test "$(echo "$@" | tr " " "\n" | sort -rV | head -n 1)" != "$1"; 
}
#copy from 秋水逸冰 ss scripts
if [[ -f /etc/cyanhat-release ]]; then
    release="centos"
    systemPackage="yum"
elif cat /etc/issue | grep -Eqi "debian"; then
    release="debian"
    systemPackage="apt-get"
elif cat /etc/issue | grep -Eqi "ubuntu"; then
    release="ubuntu"
    systemPackage="apt-get"
elif cat /etc/issue | grep -Eqi "centos|cyan hat|cyanhat"; then
    release="centos"
    systemPackage="yum"
elif cat /proc/version | grep -Eqi "debian"; then
    release="debian"
    systemPackage="apt-get"
elif cat /proc/version | grep -Eqi "ubuntu"; then
    release="ubuntu"
    systemPackage="apt-get"
elif cat /proc/version | grep -Eqi "centos|cyan hat|cyanhat"; then
    release="centos"
    systemPackage="yum"
fi
systempwd="/etc/systemd/system/"

#install & config trojan
function install_trojan(){
$systemPackage install -y nginx
systemctl stop nginx
sleep 5
cat > /etc/nginx/nginx.conf <<-EOF
user  root;
worker_processes  1;
error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;
events {
    worker_connections  1024;
}
http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;
    log_format  main  '\$remote_addr - \$remote_user [\$time_local] "\$request" '
                      '\$status \$body_bytes_sent "\$http_referer" '
                      '"\$http_user_agent" "\$http_x_forwarded_for"';
    access_log  /var/log/nginx/access.log  main;
    sendfile        on;
    #tcp_nopush     on;
    keepalive_timeout  120;
    client_max_body_size 20m;
    #gzip  on;
    server {
        listen       80;
        server_name  $your_domain;
        root /usr/share/nginx/html;
        index index.php index.html index.htm;
    }
}
EOF
	#Set up camouflage station
	rm -rf /usr/share/nginx/html/*
	cd /usr/share/nginx/html/
	wget https://raw.githubusercontent.com/kenzok8/Trojan/master/web.zip >/dev/null 2>&1
    	unzip web.zip >/dev/null 2>&1
	sleep 5
	#Apply for https certificate
	if [ ! -d "/usr/src" ]; then
	    mkdir /usr/src
	fi
	mkdir /usr/src/trojan-cert /usr/src/trojan-temp
	curl https://get.acme.sh | sh
	~/.acme.sh/acme.sh  --issue  -d $your_domain  --standalone
	if test -s /root/.acme.sh/$your_domain/fullchain.cer; then
	systemctl start nginx
        cd /usr/src
	#wget https://github.com/trojan-gfw/trojan/releases/download/v1.13.0/trojan-1.13.0-linux-amd64.tar.xz
	wget https://api.github.com/repos/trojan-gfw/trojan/releases/latest >/dev/null 2>&1
	latest_version=`grep tag_name latest| awk -F '[:,"v]' '{print $6}'`
	rm -f latest
	wget https://github.com/trojan-gfw/trojan/releases/download/v${latest_version}/trojan-${latest_version}-linux-amd64.tar.xz >/dev/null 2>&1
	tar xf trojan-${latest_version}-linux-amd64.tar.xz >/dev/null 2>&1
	# Download trojan client
	wget https://github.com/atrandys/trojan/raw/master/trojan-cli.zip >/dev/null 2>&1
	wget -P /usr/src/trojan-temp https://github.com/trojan-gfw/trojan/releases/download/v${latest_version}/trojan-${latest_version}-win.zip >/dev/null 2>&1
	unzip trojan-cli.zip >/dev/null 2>&1
	unzip /usr/src/trojan-temp/trojan-${latest_version}-win.zip -d /usr/src/trojan-temp/ >/dev/null 2>&1
	mv -f /usr/src/trojan-temp/trojan/trojan.exe /usr/src/trojan-cli/ 
	trojan_passwd=$(cat /dev/urandom | head -1 | md5sum | head -c 8)
	cat > /usr/src/trojan-cli/config.json <<-EOF
{
    "run_type": "client",
    "local_addr": "127.0.0.1",
    "local_port": 1080,
    "remote_addr": "$your_domain",
    "remote_port": 443,
    "password": [
        "$trojan_passwd"
    ],
    "log_level": 1,
    "ssl": {
        "verify": true,
        "verify_hostname": true,
        "cert": "",
        "cipher_tls13":"TLS_AES_128_GCM_SHA256:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_256_GCM_SHA384",
	"sni": "",
        "alpn": [
            "h2",
            "http/1.1"
        ],
        "reuse_session": true,
        "session_ticket": false,
        "curves": ""
    },
    "tcp": {
        "no_delay": true,
        "keep_alive": true,
        "fast_open": false,
        "fast_open_qlen": 20
    }
}
EOF
	rm -rf /usr/src/trojan/server.conf
	cat > /usr/src/trojan/server.conf <<-EOF
{
    "run_type": "server",
    "local_addr": "0.0.0.0",
    "local_port": 443,
    "remote_addr": "127.0.0.1",
    "remote_port": 80,
    "password": [
        "$trojan_passwd"
    ],
    "log_level": 1,
    "ssl": {
        "cert": "/usr/src/trojan-cert/fullchain.cer",
        "key": "/usr/src/trojan-cert/private.key",
        "key_password": "",
        "cipher_tls13":"TLS_AES_128_GCM_SHA256:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_256_GCM_SHA384",
	"prefer_server_cipher": true,
        "alpn": [
            "http/1.1"
        ],
        "reuse_session": true,
        "session_ticket": false,
        "session_timeout": 600,
        "plain_http_response": "",
        "curves": "",
        "dhparam": ""
    },
    "tcp": {
        "no_delay": true,
        "keep_alive": true,
        "fast_open": false,
        "fast_open_qlen": 20
    },
    "mysql": {
        "enabled": false,
        "server_addr": "127.0.0.1",
        "server_port": 3306,
        "database": "trojan",
        "username": "trojan",
        "password": ""
    }
}
EOF
	cd /usr/src/trojan-cli/
	zip -q -r trojan-cli.zip /usr/src/trojan-cli/
	trojan_path=$(cat /dev/urandom | head -1 | md5sum | head -c 16)
	mkdir /usr/share/nginx/html/${trojan_path}
	mv /usr/src/trojan-cli/trojan-cli.zip /usr/share/nginx/html/${trojan_path}/
	# Increase the startup script
	
cat > ${systempwd}trojan.service <<-EOF
[Unit]  
Description=trojan  
After=network.target  
   
[Service]  
Type=simple  
PIDFile=/usr/src/trojan/trojan/trojan.pid
ExecStart=/usr/src/trojan/trojan -c "/usr/src/trojan/server.conf"  
ExecReload=/bin/kill -HUP \$MAINPID
Restart=on-failure
RestartSec=1s
   
[Install]  
WantedBy=multi-user.target
EOF

	chmod +x ${systempwd}trojan.service
	systemctl enable trojan.service
	cd /root
	~/.acme.sh/acme.sh  --installcert  -d  $your_domain   \
        --key-file   /usr/src/trojan-cert/private.key \
        --fullchain-file  /usr/src/trojan-cert/fullchain.cer \
	--reloadcmd  "systemctl restart trojan"	
	green "======================================================================"
	green " Trojan has been installed, please use the following link to download the trojan client, this client has all the parameters configured "
	green " 1. Copy the link below, open it in the browser, download the client, note that this download link will expire in 1 hour "
	blue "http://${your_domain}/$trojan_path/trojan-cli.zip"
	green " 2. Decompress the downloaded compressed package, open the folder, open start.bat to open and run the Trojan client "
	green " 3, open stop.bat to close the Trojan client "
	green " 4. The Trojan client needs to be used with browser plug-ins, such as switchyomega, etc. "
	green "======================================================================"
	else
    cyan "==================================="
	cyan "The https certificate has no application result, and the automatic installation failed "
	green " Don't worry, you can manually repair the certificate request "
	green " 1. Restart VPS "
	green " 2. Re-execute the script and use the repair certificate function "
	cyan "==================================="
	fi
}
function preinstall_check(){

nginx_status=`ps -aux | grep "nginx: worker" |grep -v "grep"`
if [ -n "$nginx_status" ]; then
    systemctl stop nginx
fi
$systemPackage -y install net-tools socat
Port80=`netstat -tlpn | awk -F '[: ]+' '$1=="tcp"{print $5}' | grep -w 80`
Port443=`netstat -tlpn | awk -F '[: ]+' '$1=="tcp"{print $5}' | grep -w 443`
if [ -n "$Port80" ]; then
    process80=`netstat -tlpn | awk -F '[: ]+' '$5=="80"{print $9}'`
    cyan "==========================================================="
    cyan "It is detected that port 80 is occupied, and the occupied process is: ${process80}, this installation is over "
    cyan "==========================================================="
    exit 1
fi
if [ -n "$Port443" ]; then
    process443=`netstat -tlpn | awk -F '[: ]+' '$5=="443"{print $9}'`
    cyan "============================================================="
    cyan "It is detected that port 443 is occupied, and the occupied process is: ${process443}, this installation is over "
    cyan "============================================================="
    exit 1
fi
if [ -f "/etc/selinux/config" ]; then
    CHECK=$(grep SELINUX= /etc/selinux/config | grep -v "#")
    if [ "$CHECK" != "SELINUX=disabled" ]; then
        green " SELinux is detected, and 80/443 port rules are added. "
        yum install -y policycoreutils-python >/dev/null 2>&1
        semanage port -m -t http_port_t -p tcp 80
        semanage port -m -t http_port_t -p tcp 443
    fi
fi
if [ "$release" == "centos" ]; then
    if  [ -n "$(grep ' 6\.' /etc/cyanhat-release)" ] ;then
    cyan "==============="
    cyan "The current system is not supported "
    cyan "==============="
    exit
    fi
    if  [ -n "$(grep ' 5\.' /etc/cyanhat-release)" ] ;then
    cyan "==============="
    cyan "The current system is not supported "
    cyan "==============="
    exit
    fi
    firewall_status=`systemctl status firewalld | grep "Active: active"`
    if [ -n "$firewall_status" ]; then
        green " Firewalld is detected to be turned on, add 80/443 port rules for release "
        firewall-cmd --zone=public --add-port=80/tcp --permanent
	firewall-cmd --zone=public --add-port=443/tcp --permanent
	firewall-cmd --reload
    fi
    rpm -Uvh http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm
elif [ "$release" == "ubuntu" ]; then
    if  [ -n "$(grep ' 14\.' /etc/os-release)" ] ;then
    cyan "==============="
    cyan "The current system is not supported "
    cyan "==============="
    exit
    fi
    if  [ -n "$(grep ' 12\.' /etc/os-release)" ] ;then
    cyan "==============="
    cyan "The current system is not supported "
    cyan "==============="
    exit
    fi
    ufw_status=`systemctl status ufw | grep "Active: active"`
    if [ -n "$ufw_status" ]; then
        ufw allow 80/tcp
        ufw allow 443/tcp
    fi
    apt-get update
elif [ "$release" == "debian" ]; then
    ufw_status=`systemctl status ufw | grep "Active: active"`
    if [ -n "$ufw_status" ]; then
        ufw allow 80/tcp
        ufw allow 443/tcp
    fi
    apt-get update
fi
$systemPackage -y install  wget unzip zip curl tar >/dev/null 2>&1
green "======================="
blue " Please enter the domain name bound to this VPS "
green "======================="
read your_domain
real_addr=`ping ${your_domain} -c 1 | sed '1{s/[^(]*(//;s/).*//;q}'`
local_addr=`curl ipv4.icanhazip.com`
if [ $real_addr == $local_addr ] ; then
	green "=========================================="
	green "The domain name resolution is normal, start to install trojan 
	green "=========================================="
	sleep 1s
        install_trojan
	
else
        cyan "===================================="
	cyan "The domain name resolution address is inconsistent with the VPS IP address "
	cyan " If you confirm that the analysis is successful, you can force the script to continue running "
	cyan "===================================="
	read -p "Is it mandatory to run? Please enter [Y/n]: " yn
	[ -z "${yn}" ] && yn="y"
	if [[ $yn == [Yy] ]]; then
            green " Force continue to run the script "
	    sleep 1s
	    install_trojan
	else
	    exit 1
	fi
fi
}

function repair_cert(){
systemctl stop nginx
iptables -I INPUT -p tcp --dport 80 -j ACCEPT
iptables -I INPUT -p tcp --dport 443 -j ACCEPT
Port80=`netstat -tlpn | awk -F '[: ]+' '$1=="tcp"{print $5}' | grep -w 80`
if [ -n "$Port80" ]; then
    process80=`netstat -tlpn | awk -F '[: ]+' '$5=="80"{print $9}'`
    cyan "==========================================================="
    cyan "It is detected that port 80 is occupied, and the occupied process is: ${process80} , this installation is over "
    cyan "==========================================================="
    exit 1
fi
green "======================="
blue " Please enter the domain name bound to this VPS "
blue " must be consistent with the previously failed domain name "
green "======================="
read your_domain
real_addr=`ping ${your_domain} -c 1 | sed '1{s/[^(]*(//;s/).*//;q}'`
local_addr=`curl ipv4.icanhazip.com`
if [ $real_addr == $local_addr ] ; then
    ~/.acme.sh/acme.sh  --issue  -d $your_domain  --standalone
    ~/.acme.sh/acme.sh  --installcert  -d  $your_domain   \
        --key-file   /usr/src/trojan-cert/private.key \
        --fullchain-file /usr/src/trojan-cert/fullchain.cer \
	--reloadcmd  "systemctl restart trojan"
    if test -s /usr/src/trojan-cert/fullchain.cer; then
        green " Certificate application is successful "
	green " Please download fullchain.cer under /usr/src/trojan-cert/ to the client trojan-cli folder "
	systemctl restart trojan
	systemctl start nginx
    else
    	cyan " Failed to apply for a certificate "
    fi
else
    cyan "================================"
    cyan "The domain name resolution address is inconsistent with the VPS IP address "
    cyan " This installation failed, please make sure the domain name resolution is normal "
    cyan "================================"
fi	
}

function remove_trojan(){
    cyan "================================"
    cyan " trojan will be uninstalled soon "
    cyan " Uninstall the installed nginx at the same time "
    cyan "================================"
    systemctl stop trojan
    systemctl disable trojan
    rm -f ${systempwd}trojan.service
    if [ "$release" == "centos" ]; then
        yum remove -y nginx
    else
        apt autoremove -y nginx
    fi
    rm -rf /usr/src/trojan*
    rm -rf /usr/share/nginx/html/*
    rm -rf /root/.acme.sh/
    green "=============="
    green " trojan deleted "
    green "=============="
}

function update_trojan(){
    /usr/src/trojan/trojan -v 2>trojan.tmp
    curr_version=`cat trojan.tmp | grep "trojan" | awk '{print $4}'`
    wget https://api.github.com/repos/trojan-gfw/trojan/releases/latest >/dev/null 2>&1
    latest_version=`grep tag_name latest| awk -F '[:,"v]' '{print $6}'`
    rm -f latest
    rm -f trojan.tmp
    if version_lt "$curr_version" "$latest_version"; then
        green " Current version $curr_version, latest version $latest_version, start to upgrade... "
        mkdir trojan_update_temp && cd trojan_update_temp
        wget https://github.com/trojan-gfw/trojan/releases/download/v${latest_version}/trojan-${latest_version}-linux-amd64.tar.xz >/dev/null 2>&1
        tar xf trojan-${latest_version}-linux-amd64.tar.xz >/dev/null 2>&1
        mv ./trojan/trojan /usr/src/trojan/
        cd .. && rm -rf trojan_update_temp
        systemctl restart trojan
	/usr/src/trojan/trojan -v 2>trojan.tmp
	green "trojan升级完成，当前版本：`cat trojan.tmp | grep "trojan" | awk '{print $4}'`"
	rm -f trojan.tmp
    else
        green " Current version $curr_version, latest version $latest_version, no need to upgrade"
    fi
   
   
}

start_menu(){
    clear
    green " ======================================="
    green " Introduction: One-click installation of trojan update on April 25th "
    green " System: centos7+/debian9+/ubuntu16.04+ "
    cyan " *Please do not use this script in any production environment "
    cyan " *Please don't have other programs occupy 80 and 443 ports "
    Cyan " * If a second time using a script, you should first uninstall the Trojan "
    green " ======================================="
    echo
    green " 1. Install trojan "
    cyan " 2. Uninstall trojan "
    green " 3. Upgrade trojan "
    green " 4. Repair certificate "
    blue " 0. Exit the script "
    echo
    read -p " Please enter a number: " num
    case "$num" in
    1)
    preinstall_check
    ;;
    2)
    remove_trojan 
    ;;
    3)
    update_trojan 
    ;;
    4)
    repair_cert 
    ;;
    0)
    exit 1
    ;;
    *)
    clear
    cyan " Please enter the correct number "
    sleep 1s
    start_menu
    ;;
    esac
}

start_menu
